package data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDTO {
	
	private String username;
	private boolean isAdmin;
	private String dateOfBirth;
	private String email;
	private String name;
	private String password;
	private String superpower;
		
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	@JsonProperty("dateOfBirth")
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getEmail() {
		return email;
	}
	
	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}
		
	public boolean isAdmin() {
		return isAdmin;
	}
	
	@JsonProperty("isAdmin")
	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public String getName() {
		return name;
	}
	
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}
	
	@JsonProperty("password")
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getSuperpower() {
		return superpower;
	}
	
	@JsonProperty("superpower")
	public void setSuperpower(String superpower) {
		this.superpower = superpower;
	}
	
	public String getUsername() {
		return username;
	}
	
	@JsonProperty("username")
	public void setUsername(String username) {
		this.username = username;
	}

	public static UserDTO createUserDto(String dateOfBirth, String email, String name, String password,
			String superpower, String username, boolean isAdmin) {
		UserDTO user = new UserDTO();
		
		user.setDateOfBirth(dateOfBirth);
		user.setEmail(email);
		user.setIsAdmin(isAdmin);
		user.setName(name);
		user.setPassword(password);
		user.setSuperpower(superpower);
		user.setUsername(username);
		
		return user;
	}
	
	@Override
	public String toString() {
		return "{\"dateOfBirth\":" + "\"" + dateOfBirth + "\"" + ", \"email\":" + "\"" + email + "\"" + ", \"isAdmin\":"
				+ isAdmin + ",\"name\":" + "\"" + name + "\"" + ", \"password\":" + "\"" + password + "\""
				+ ", \"superpower\":" + "\"" + superpower + "\"" + ", \"username\":" + "\"" + username + "\"" + "}";
	}
}
