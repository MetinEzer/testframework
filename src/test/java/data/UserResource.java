package data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserResource {
	private String dateOfBirth;
	private String email;
	private int id;
	private boolean isAdmin;
	private String name;
	private String superpower;
	private String username;
	private String password;

	
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	@JsonProperty("dateOfBirth")
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getEmail() {
		return email;
	}
	
	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getId() {
		return id;
	}
	
	@JsonProperty("id")
	public void setId(int id) {
		this.id = id;
	}
	
	public boolean isAdmin() {
		return isAdmin;
	}
	
	@JsonProperty("isAdmin")
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public String getName() {
		return name;
	}
	
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSuperpower() {
		return superpower;
	}
	
	@JsonProperty("superpower")
	public void setSuperpower(String superpower) {
		this.superpower = superpower;
	}
	
	public String getUsername() {
		return username;
	}
	
	@JsonProperty("username")
	public void setUsername(String username) {
		this.username = username;
	}
	

	public String getPassword() {
		return password;
	}
	
	@JsonProperty("password")
	public void setPassword(String password) {
		this.password = password;
	}

	public static UserResource createUserResource(String dateOfBirth, String email, String name, String password,
			String superpower, String username, boolean isAdmin, int id) {
		UserResource user = new UserResource();
		
		user.setDateOfBirth(dateOfBirth);
		user.setEmail(email);
		user.setAdmin(isAdmin);
		user.setName(name);
		user.setPassword(password);
		user.setSuperpower(superpower);
		user.setUsername(username);
		user.setId(id);
		
		return user;
	}

	
	@Override
	public String toString() {
		return "{\"dateOfBirth\":" + "\"" + dateOfBirth + "\"" + ", \"email\":" + "\"" + email + "\"" + ", \"isAdmin\":"
		+ isAdmin + ",\"name\":" + "\"" + name + "\"" + ", \"password\":" + "\"" + password + "\""
		+ ", \"superpower\":" + "\"" + superpower + "\"" + ", \"username\":" + "\"" + username + "\"" + ", \"id\":" + "\"" + id + "\"" + "}";

	}
}