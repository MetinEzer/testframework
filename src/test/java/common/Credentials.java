package common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Credentials {
	private final String adminUser;
	private final String adminPassword;
	private final String testUser;
	private final String testPassword;
	private final String devUser;
	private final String devPassword;
	private final String wrongUser;
	private final String wrongPassword;
	
	public Credentials() throws FileNotFoundException, IOException {
		super();
		InputStream in = this.getClass().getResourceAsStream("/config.properties");
		Properties credentials = new Properties();
		credentials.load(in);
		
		adminUser = credentials.getProperty("admin.username");
		testUser = credentials.getProperty("test.username");
		devUser = credentials.getProperty("dev.username");
		wrongUser = credentials.getProperty("wrong.username");
		
		adminPassword = credentials.getProperty("admin.password");
		testPassword = credentials.getProperty("test.password");
		devPassword = credentials.getProperty("dev.password");
		wrongPassword = credentials.getProperty("wrong.password");
	}
	
	public String getAdminUser() {
		return adminUser;
	}
	public String getAdminPassword() {
		return adminPassword;
	}
	public String getTestUser() {
		return testUser;
	}
	public String getTestPassword() {
		return testPassword;
	}
	public String getDevUser() {
		return devUser;
	}
	public String getDevPassword() {
		return devPassword;
	}

	public String getWrongUser() {
		return wrongUser;
	}

	public String getWrongPassword() {
		return wrongPassword;
	}
}
