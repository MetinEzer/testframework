package testpackage;

import static io.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import common.URIConfig;
import data.UserDTO;
import data.UserResource;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


public class CreateNewUserTests extends TestBase {
	
	private final UserDTO newUser = UserDTO.createUserDto("1934-11-09", "carl@sagan.com", "Carl", "42", "Time Traveller", "carl_sagan", true);
	protected Response response;
	protected UserResource responseResource;

	
	@Test(description = "Create user returns 201 Created")
	public void createUserReturnCreated() {
		LOG.info("@Test: createUserReturnCreated");

		LOG.info("@TestStep: Create user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
	            .accept(ContentType.JSON)
	            .body(newUser)
				.when()
				.post(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_CREATED)
				.extract().response();
		response.prettyPrint();

		responseResource = response.getBody().as(UserResource.class);
		Assert.assertNotNull(responseResource, "Resource ojbect is null");
		
		LOG.info("@TestStep: Check if user is created.");
		response = given().log().all()
				.queryParam("username", responseResource.getUsername())
				.when()
				.get(URIConfig.BASE_URL + URIConfig.DETAILS)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
		response.prettyPrint();

		responseResource = response.getBody().as(UserResource.class);
		Assert.assertTrue(responseResource.getUsername().equals(newUser.getUsername()), "Wrong user returned!");
		
		LOG.info("@TestStep: Delete created user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept(ContentType.TEXT)
				.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
				.body(newUser)
				.when()
				.delete(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();	
		response.prettyPrint();
	}

	@Test(description = "Create isAdmin=true user and check if it is set to false.")
	public void craeteAdminUserAndReturnOkAndCheckFalse() {
		LOG.info("@Test: craeteAdminUserAndReturnOkAndCheckFalse");

		LOG.info("@TestStep: Craete user with isAdmin=true flag.");
		response = given().log().all()
				.contentType(ContentType.JSON)
	            .accept(ContentType.JSON)
	            .body(newUser)
				.when()
				.post(URIConfig.BASE_URL)
				.then()	.log().status()
				.statusCode(HttpStatus.SC_CREATED)
				.extract().response();
		response.prettyPrint();

		responseResource = response.getBody().as(UserResource.class);
		Assert.assertNotNull(responseResource, "Resource ojbect is null");
		
		LOG.info("@TestStep: Get created user and check if admin flad set to false.");
		response = given()
				.queryParam("username", responseResource.getUsername())
				.when()
				.get(URIConfig.BASE_URL + URIConfig.DETAILS)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
		response.prettyPrint();
				
		responseResource = response.getBody().as(UserResource.class);
		Assert.assertTrue(!responseResource.isAdmin(), "User should not be created as admin initially!");
		
		LOG.info("@TestStep: Delete created user.");
		response = given()
				.contentType(ContentType.JSON)
				.accept(ContentType.TEXT)
				.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
				.body(newUser)
				.when()
				.delete(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
		response.prettyPrint();

	}
	
	@Test(description = "Create user with wrong dataType and data, expect 400", dataProvider = "provideWrongTestData")
	public void createUserWithWrongDataAnDataTypeReturnBadRequest(String wrongBody) {
		LOG.info("@Test: createUserWithWrongDataAnDataTypeReturnBadRequest");

		LOG.info("@TestStep: Create user.");
		response =	given().log().all()
				.contentType(ContentType.JSON)
			    .accept("*/*")
			    .body(wrongBody)
				.when()
				.post("http://localhost:8081/waesheroes/api/v1/users")
				.then().log().status()
				.statusCode(400)
				.extract().response();
		response.prettyPrint();

	}
	
	@Test(description = "Create new user and than try to create again.")
	public void createExistingUserReturnsBadRequest() {
		LOG.info("@Test: createExistingUserReturnsBadRequest");

		LOG.info("@TestStep: Create user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
	            .accept(ContentType.JSON)
	            .body(newUser)
				.when()
				.post(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_CREATED)
				.extract().response();
		response.prettyPrint();

		responseResource = response.getBody().as(UserResource.class);
		Assert.assertNotNull(responseResource, "Resource ojbect is null");
		
		LOG.info("@TestStep: Try to create exitsting user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
	            .accept(ContentType.JSON)
	            .body(newUser)
				.when()
				.post(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_FORBIDDEN)
				.extract().response();
		response.prettyPrint();
		
		LOG.info("@TestStep: Delete created user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept(ContentType.TEXT)
				.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
				.body(newUser)
				.when()
				.delete(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
		response.prettyPrint();	
	}
	
	@DataProvider
	public String[] provideWrongTestData() {
		return new String[] {"{\"dateOfBirth\":\"19431109\", \"email\":\"carl@sagan.com\", \"isAdmin\":false,\"name\":\"Carl\", \"password\":\"42\", \"superpower\":\"Time Traveller\", \"username\":\"carl_sagan\"}",
				"{\"dateOfBirth\":1934, \"email\":\"carl2@sagan.com\", \"isAdmin\":false,\"name\":\"Carl\", \"password\":\"42\", \"superpower\":\"Time Traveller\", \"username\":\"carl_sagan1\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\":\"carlsagancom\", \"isAdmin\":false,\"name\":\"Carl\", \"password\":\"42\", \"superpower\":\"Time Traveller\", \"username\":\"carl_sagan2\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\": 12, \"isAdmin\":false,\"name\":\"Carl\", \"password\":\"42\", \"superpower\":\"Time Traveller\", \"username\":\"carl_sagan3\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\":\"carl3@sagan.com\", \"isAdmin\":false,\"name\":\"1234\", \"password\":\"42\", \"superpower\":\"Time Traveller\", \"username\":\"carl_sagan4\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\":\"carl4@sagan.com\", \"isAdmin\":false,\"name\":\"Carl\", \"password\":\"000000000000000000000000000000000000000000000000000000042\", \"superpower\":\"Time Traveller\", \"username\":\"carl_sagan5\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\":\"carl5@sagan.com\", \"isAdmin\":false,\"name\":\"Carl\", \"password\":42, \"superpower\":\"Time Traveller\", \"username\":\"carl_sagan6\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\":\"carl6@sagan.com\", \"isAdmin\":false,\"name\":\"Carl\", \"password\":\"0\", \"superpower\":\"Time Traveller\", \"username\":\"carl_sagan7\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\":\"carl7@sagan.com\", \"isAdmin\":false,\"name\":\"Carl\", \"password\":\"42\", \"superpower\":\"%&(&()/()/\", \"username\":\"carl_sagan8\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\":\"carl8@sagan.com\", \"isAdmin\":false,\"name\":\"Carl\", \"password\":\"42\", \"superpower\": 12, \"username\":\"carl_sagan9\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\":\"carl9@sagan.com\", \"isAdmin\":false,\"name\":\"Carl\", \"password\":\"42\", \"superpower\":\"Time Traveller\", \"username\":\"345345\"}",
				"{\"dateOfBirth\":\"1934-11-09\", \"email\":\"carl10@sagan.com\", \"isAdmin\":\"true\",\"name\":\"Carl\", \"password\":\"42\", \"superpower\":\"Time Traveller\", \"username\":\"carl_sagan10\"}"};	
	}
	
	/*Clean up created resources for failed wrongdata test*/
//	@Test(dataProvider = "provideWrongTestData")
//	public void deleteAllWrongData(String user) {
//			given()
//			.contentType(ContentType.JSON)
//			.accept(ContentType.TEXT)
//			.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
//			.body(user)
//			.when()
//			.delete("http://localhost:8081/waesheroes/api/v1/users")
//			.then()
//			.statusCode(200);	
//	}
}
