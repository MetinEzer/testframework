package testpackage;

import static io.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

import common.URIConfig;
import data.UserResource;
import io.restassured.response.Response;

public class GetAllUsersTests extends TestBase{

	private Response response;

	@Test(description = "Get all users and receive 200 OK.")
	public void getAllUsersReturnOK() {
		LOG.info("@Test: getAllUsersReturnOK");

		LOG.info("@TestStep: Get all users.");
		response = given().log().all()
				.auth().preemptive().basic(credentials.getAdminUser(), credentials.getAdminPassword())
				.when()
				.get(URIConfig.BASE_URL + URIConfig.ALL)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
		response.prettyPrint();

		UserResource[] resources = response.getBody().as(UserResource[].class);
		Assert.assertTrue(resources.length == 3 , "User list is empty!");
	}
	
	@Test(description = "Get all users with non existing user and receive 401 Unauthorized.")
	public void getAllUsersReturnUnauthorizedForNonExistingUser() {
		LOG.info("@Test: getAllUsersReturnUnauthorizedForNonExistingUser");

		LOG.info("@TestStep: Get all users.");
		response = given().log().all()
	        .auth().preemptive().basic(credentials.getWrongUser(), credentials.getWrongPassword())
	        .when()
	        .get(URIConfig.BASE_URL + URIConfig.ALL)
	        .then().log().status()
			.statusCode(HttpStatus.SC_UNAUTHORIZED)
			.extract().response();
		response.prettyPrint();
	}

	@Test(description = "Get all users with unauthorized user and receive 403 Forbidden")
	public void getAllUsersReturnForbiddenForNotAdminUser() {
		LOG.info("@Test: getAllUsersReturnForbiddenForNotAdminUser");

		LOG.info("@TestStep: Get all users.");
		response = given().log().all()
	        .auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
	        .when()
	        .get(URIConfig.BASE_URL + URIConfig.ALL)
	        .then().log().status()
			.statusCode(HttpStatus.SC_FORBIDDEN)
			.extract().response();
		response.prettyPrint();
	}
	
}
