package testpackage;

import static io.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import common.URIConfig;
import data.UserDTO;
import data.UserResource;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class UpdateUserTests extends TestBase{

	private final UserDTO newUser = UserDTO.createUserDto("1943-12-03", "jim@morisson.com", "Jim", "lizardKing", "Roll a stone", "jim_morisson", false);
	
	protected Response response;
	protected UserResource responseResource;

	@BeforeMethod
	public void createResource() {
		
		LOG.info("@BeforeMethod: create resource");
		response = given().log().all()
				.contentType(ContentType.JSON)
	            .accept(ContentType.JSON)
	            .body(newUser)
				.when()
				.post(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_CREATED)
				.extract().response();
		response.prettyPrint();
		
		responseResource = response.getBody().as(UserResource.class);
	}
	
	@Test(description = "Try to update user fields successfully.", dataProvider = "userUpdateCorrectData")
	public void updateUserFieldsRetursOK(UserResource updateResource) {
		LOG.info("@Test: updateUserFieldsRetursOK");

		updateResource.setId(responseResource.getId());
		
		LOG.info("@TestStep: Update user info.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
				.auth().preemptive().basic(credentials.getDevUser(), credentials.getDevPassword())
				.body(updateResource)
				.when()
				.put(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
		response.prettyPrint();
		
		responseResource = response.getBody().as(UserResource.class);
		updateResource.setPassword(responseResource.getPassword());
		
		Assert.assertEquals(updateResource.toString(),responseResource.toString(), "Updated user information not correct");
		LOG.info("Updated user information is correct!");
	}
	
	@Test(description = "Try to update username field and expect 400 Bad Request.")
	public void updateUserNameReturnsBadRequest() {
		LOG.info("@Test: updateUserNameReturnsBadRequest");

		UserResource wrongUserName = UserResource.createUserResource("1943-12-03", "jim@morisson.com", "Jim", "lizardKing", "Open the Doors", "jim_morissonUpdate", false, responseResource.getId());
		
		LOG.info("TestStep: Update user info.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
				.auth().preemptive().basic(credentials.getDevUser(), credentials.getDevPassword())
				.body(wrongUserName)
				.when()
				.put(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.extract().response();
		response.prettyPrint();
	}
	
	@Test(description = "Update user with Unauthorized user and returns forbidden")
	public void updateUserWithUnauthorizedUserReturnsForbidden() {
		LOG.info("@Test: updateUserWithUnauthorizedUserReturnsForbidden");

		UserResource payload = UserResource.createUserResource("1943-12-03", "jim@morisson.com", "Jim Morisson", "lizardKing", "Open the Doors", "jim_morisson", false, responseResource.getId());
		
		LOG.info("TestStep: Update user info.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
				.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
				.body(payload)
				.when()
				.put(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_FORBIDDEN)
				.extract().response();
		response.prettyPrint();
	}
	
	@Test(description = "Update user with not existed user and returns unauthorized")
	public void updateUserNameWithNotExistUserReturnsUnauthorized() {
		LOG.info("@Test: updateUserNameWithNotExistUserReturnsUnauthorized");

		UserResource payload = UserResource.createUserResource("1943-12-03", "jim@morisson.com", "Jim Morisson", "lizardKing", "Open the Doors", "jim_morisson", false, responseResource.getId());

		LOG.info("TestStep: Update user info.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
				.auth().preemptive().basic(credentials.getWrongUser(), credentials.getWrongPassword())
				.body(payload)
				.when()
				.put(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_UNAUTHORIZED)
				.extract().response();
		response.prettyPrint();
	}
	
	@AfterMethod
	public void deleteCraetedUser() {
		LOG.info("@AfterMethod: clean resources.");

		response = given().log().all()
		.contentType(ContentType.JSON)
		.accept(ContentType.TEXT)
		.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
		.body(responseResource)
		.when()
		.delete(URIConfig.BASE_URL)
		.then().log().status()
		.statusCode(HttpStatus.SC_OK)
		.extract().response();
		response.prettyPrint();
	}
	
	@DataProvider
	private UserResource[] userUpdateCorrectData() {	
		return new UserResource[] {UserResource.createUserResource("1943-12-04", "jim@morisson.com", "Jim", "lizardKing", "Open the Doors", "jim_morisson", false, 0),
			UserResource.createUserResource("1943-12-03", "jimUpdate@morisson.com", "Jim", "lizardKing", "Open the Doors", "jim_morisson", false, 0),
			UserResource.createUserResource("1943-12-03", "jim@morisson.com", "JimUpdate", "lizardKing", "Open the Doors", "jim_morisson", false, 0),
			UserResource.createUserResource("1943-12-03", "jim@morisson.com", "Jim", "lizardKingUpdate", "Open the Doors", "jim_morisson", false, 0),
			UserResource.createUserResource("1943-12-03", "jim@morisson.com", "Jim", "lizardKing", "Open the DoorsUpdate", "jim_morisson", false, 0),
			UserResource.createUserResource("1943-12-03", "jim@morisson.com", "Jim", "lizardKing", "Open the Doors", "jim_morisson", true, 0)};	
	}	
	
}
	
