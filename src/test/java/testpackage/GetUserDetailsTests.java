package testpackage;

import static io.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

import common.URIConfig;
import data.UserResource;
import io.restassured.response.Response;

public class GetUserDetailsTests extends TestBase {

	private Response response;
	private UserResource resource;
	
	@Test(description = "Get user detail and return 200 OK")
	public void getUserDetailReturnOK() {
		LOG.info("@Test: getUserDetailReturnOK");

		LOG.info("@TestStep: Get user detail.");
		response = given().log().all()
				.queryParam("username", "dev")
				.auth().preemptive().basic(credentials.getAdminUser(), credentials.getAdminPassword())
				.when()
				.get(URIConfig.BASE_URL + URIConfig.DETAILS)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
			response.prettyPrint();

		resource = response.getBody().as(UserResource.class);
		
		Assert.assertNotNull(resource, "Resource ojbect is null");
		Assert.assertTrue(resource.getUsername().equals("dev"), "Wrong user returned!");
	}

	@Test(description = "Get user detail without Basic Auhtentication and return 401 Unautharized")
	public void getUserDetailReturnUnauthorizedWithoutBasicAuth() {
		LOG.info("@Test: getUserDetailReturnUnauthorizedWithoutBasicAuth");

		LOG.info("@TestStep: Get user detail.");
		response = given().log().all()
				.queryParam("username", "dev")
				.when()
				.get(URIConfig.BASE_URL + URIConfig.DETAILS)
				.then()		.log().status()
				.statusCode(HttpStatus.SC_UNAUTHORIZED)
				.extract().response();
			response.prettyPrint();
	}
	
	@Test(description = "Get user detail via unauthorized user and return 403 Forbidden")
	public void getUserDetailReturnForbiddenForUnauthorizedUser() {
		LOG.info("@Test: getUserDetailReturnForbiddenForUnauthorizedUser");

		LOG.info("@TestStep: Get user detail.");
		response = given().log().all()
				.queryParam("username", "dev")
				.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
				.when()
				.get(URIConfig.BASE_URL + URIConfig.DETAILS)
				.then().log().status()
				.statusCode(HttpStatus.SC_FORBIDDEN)
				.extract().response();
			response.prettyPrint();
	}

	@Test(description = "Get user detail via non existing user and return 401 Unauthorized")
	public void getAllUsersReturnUnauthorizedForNonExistingUser() {
		LOG.info("@Test: getAllUsersReturnUnauthorizedForNonExistingUser");

		LOG.info("@TestStep: Get user detail.");
		response = given().log().all()
				.queryParam("username", "dev")
				.auth().preemptive().basic(credentials.getWrongUser(), credentials.getWrongPassword())
				.when()
				.get(URIConfig.BASE_URL + URIConfig.DETAILS)
				.then().log().status()
				.statusCode(HttpStatus.SC_UNAUTHORIZED)
				.extract().response();
			response.prettyPrint();
	}
	
	@Test(description = "Get user details for non existed user and return 404 Not Found")
	public void getUserDetailWithNonExistedUserReturnNotFound() {
		LOG.info("@Test: getUserDetailWithNonExistedUserReturnNotFound");

		LOG.info("@TestStep: Get user detail.");
		response = given().log().all()
				.queryParam("username", "asimovHere")
				.when()
				.get(URIConfig.BASE_URL + URIConfig.DETAILS)
				.then().log().status()
				.statusCode(HttpStatus.SC_NOT_FOUND)
				.extract().response();
			response.prettyPrint();	
	}
	
	@Test(description = "Get user details via wrong key value and return 400 Bad Request")
	public void getUserDetailWithWrongKeyValueReturnsBadRequest() {
		LOG.info("@Test: getUserDetailWithWrongKeyValueReturnsBadRequest");

		LOG.info("@TestStep: Get user detail.");
		response = given().log().all()
				.queryParam("notExistKey", "dev")
				.when()
				.get(URIConfig.BASE_URL + URIConfig.DETAILS)
				.then().log().status()
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.extract().response();
			response.prettyPrint();
	}
}
