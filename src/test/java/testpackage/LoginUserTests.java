package testpackage;

import static io.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import common.URIConfig;
import data.UserDTO;
import data.UserResource;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class LoginUserTests extends TestBase {
	
	private final UserDTO newUser = UserDTO.createUserDto("1950-11-09", "rick@sanchez.com", "Rick", "wubalubadubdub", "schwifty", "rickTheSanchez", false);
	private Response response;
	private UserResource responseResource;

	@Test(description = "Login user with correct credentials and returns 200 OK.", dataProvider = "getUserCredentials")
	public void loginUserWithCorrectCredentialsReturnsOK(String username, String password) {
		LOG.info("@Test: loginUserWithCorrectCredentialsReturnsOK");

		LOG.info("@TestStep: Login user");
		response = given().log().all()
				.accept(ContentType.JSON)
				.auth().preemptive().basic(username, password)
				.when()
				.get(URIConfig.BASE_URL + URIConfig.ACCESS)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
		
		response.prettyPrint();
	}
	
	@Test
	public void createNewUserAndLogin() {
		LOG.info("@Test: createNewUserAndLogin");

		LOG.info("@TestStep: Create user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
	            .accept(ContentType.JSON)
	            .body(newUser)
				.when()
				.post(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_CREATED)
				.extract().response();
		response.prettyPrint();

		responseResource = response.as(UserResource.class);
		
		LOG.info("@TestStep: Login created user.");
		response = given().log().uri().log().headers()
				.accept(ContentType.JSON)
				.auth().preemptive().basic(responseResource.getUsername(), newUser.getPassword())
				.when()
				.get(URIConfig.BASE_URL + URIConfig.ACCESS)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
		response.prettyPrint();
	}
	
	@Test(description = "Try to login non existing user and returns 401 Unauthorized")
	public void loginNonExistingUserAndReturnsUnauthorized() {
		LOG.info("@Test: createNewUserAndLogin");
		
		LOG.info("@TestStep: Login non existing user.");
		response = given().log().uri().log().headers()
				.accept(ContentType.JSON)
				.auth().preemptive().basic(credentials.getWrongUser(), credentials.getWrongPassword())
				.when()
				.get(URIConfig.BASE_URL + URIConfig.ACCESS)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();
		response.prettyPrint();
	}
	
	//test
	public void loginReturns403() {
		
	}
	
	//test
	public void loginReturns404() {
		
	}
	
	//test
	public void loginParametersTest() {
		
	}

	@DataProvider
	public String[][] getUserCredentials(){
		return new String[][]{{credentials.getAdminUser(),credentials.getAdminPassword()}
		,{credentials.getTestUser(),credentials.getTestPassword()}
		,{credentials.getDevUser(),credentials.getDevPassword()}};
	}
}
