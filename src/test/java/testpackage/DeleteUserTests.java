package testpackage;

import static io.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.URIConfig;
import data.UserDTO;
import data.UserResource;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class DeleteUserTests extends TestBase{
	
	private final UserDTO newUser = UserDTO.createUserDto("1920-01-02", "isaac@asimov.com", "Asimov", "meMySelfAndRobot", "area effected creativity buff", "asimovHere", false);
	private final UserResource notExistUser = UserResource.createUserResource("4242-42-42", "me@not.exist", "Hollow", "none", "nothingless", "nopass", false, 0);
	private final UserDTO dummyUser = UserDTO.createUserDto("1986-06-10", "dummy@email.com", "DummyName", "dummyPass", "dummyPower", "dummy_name", false);

	private UserResource responseResource = UserResource.createUserResource("1920-01-02", "isaac@asimov.com", "Asimov", "meMySelfAndRobot", "area effected creativity buff", "asimovHere", false, 0);
	private Response response;
	private String result;
	
	private boolean isFailScenario = false;
	private boolean isDummyExist = false;
	
	@Test(description = "Delete existing user returns 200 OK")
	public void deleteUserRetunrsOK() {
		LOG.info("@Test: deleteUserRetunrsOK");
		
		LOG.info("@TestStep: Delete created user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept(ContentType.TEXT)
				.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
				.body(newUser)
				.when()
				.delete(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();	
		response.prettyPrint();

		result= response.getBody().asString();
		Assert.assertEquals(result, "User '" + responseResource.getUsername() + "' removed from database." , "User deletion wrong status code returned then expected!");
		
		LOG.info("@TestStep: Check if created user removed from database.");
		response = given().log().all()
		.queryParam("username", newUser.getUsername())
		.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
		.when()
		.get(URIConfig.BASE_URL + URIConfig.DETAILS)
		.then().log().status()
		.extract().response();
		
		Assert.assertNotEquals(response.statusCode(), HttpStatus.SC_OK, "Deleted user can be retreived from database.");
	}
	
	@Test(description = "Delete existing user with not exist user returns 401 Unauthorized")
	public void deleteUserExistingUserWithNotExistUserRetunrsUnauthorized() {
		LOG.info("@Test: deleteUserExistingUserWithNotExistUserRetunrsUnauthorized");
		isFailScenario = true;

		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept("*/*")
				.auth().preemptive().basic(credentials.getWrongUser(), credentials.getWrongPassword())
				.body(responseResource)
				.when()
				.delete(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_UNAUTHORIZED)
				.extract().response();	
		response.prettyPrint();
	}
	
	@Test(description = "Delete existing user returns 403 Forbidden")
	public void deleteUserWithDummyCredentialsRetunrsUnauthorized() {
		LOG.info("@Test: deleteUserWithDummyCredentialsRetunrsUnauthorized");
		
		isFailScenario = true;
		isDummyExist = true;
	
		LOG.info("@TestStep: Create dummy user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
	            .accept("*/*")
	            .body(dummyUser)
				.when()
				.post(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_CREATED)
				.extract().response();	
		response.prettyPrint();
		
		LOG.info("@TestStep: Delete new user with dummy user credentials.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept("*/*")
				.auth().preemptive().basic("dummy_name", "dummyPass")
				.body(responseResource)
				.when()
				.delete(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_UNAUTHORIZED)
				.extract().response();	
		response.prettyPrint();	

	}
	
	@Test(description = "Delete non existing user returns 400 Bad Request")
	public void deleteNotExistUserRetunrsBadRequest() {
		LOG.info("@Test: deleteNotExistUserRetunrsBadRequest");

		isFailScenario = true;
		
		LOG.info("@TestStep: Delete not exist user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept("*/*")
				.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
				.body(notExistUser)
				.when()
				.delete(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.extract().response();	
		response.prettyPrint();		
	}
	
	@Test(description = "Delete user with missing user_name fields")
	public void deleteUserWithMissingUserNameRetunrsNotFound() {
		LOG.info("@Test: deleteUserWithMissingFieldsRetunrsNotFound");
		
		isFailScenario = true;
		
		LOG.info("@TestStep: Delete not exist user.");
		response = given().log().all()
				.contentType(ContentType.JSON)
				.accept("*/*")
				.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
				.body(UserResource.createUserResource("1920-01-02", "", "Asimov", "meMySelfAndRobot", "area effected creativity buff", "", false, responseResource.getId()))
				.when()
				.delete(URIConfig.BASE_URL)
				.then().log().status()
				.statusCode(HttpStatus.SC_NOT_FOUND)
				.extract().response();	
		response.prettyPrint();			
	}
	
	@BeforeMethod
	public void createResource() {
		
		LOG.info("@BeforeMethod: create resource");
		response = given().log().all()
				.queryParam("username", newUser.getUsername())
				.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
				.when()
				.get(URIConfig.BASE_URL + URIConfig.DETAILS)
				.then().log().status()
				.extract().response();	
		response.prettyPrint();			
		
		if(response.statusCode() != HttpStatus.SC_OK) {
			LOG.info("@TestStep: Create user.");
			response = given().log().all()
					.contentType(ContentType.JSON)
		            .accept(ContentType.JSON)
		            .body(newUser)
					.when()
					.post(URIConfig.BASE_URL)
					.then().log().status()
					.statusCode(HttpStatus.SC_CREATED)
					.extract().response();
			response.prettyPrint();
			
			responseResource = response.as(UserResource.class);
		}
	}


	@AfterMethod
	private void cleanUpForNegativePath()
	{
		LOG.info("@AfterMethod: Clean up for negative path tests.");

		if(isDummyExist) {
			LOG.info("Delete dummy user resource.");
			given().log().all()
			.contentType(ContentType.JSON)
			.accept(ContentType.TEXT)
			.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
			.body(dummyUser)
			.when()
			.delete("http://localhost:8081/waesheroes/api/v1/users")
			.then().log().status()
			.statusCode(HttpStatus.SC_OK)
			.extract().response();
			
			response.prettyPrint();
		}
					
		if(isFailScenario) {
			LOG.info("Delete new user resource.");
			response = given()
					.queryParam("username", newUser.getUsername())
					.when()
					.get(URIConfig.BASE_URL + URIConfig.DETAILS)
					.then()
					.extract().response();
			
			if(response.statusCode() == HttpStatus.SC_OK) {
				response = given().log().all()
						.contentType(ContentType.JSON)
						.accept("*/*")
						.auth().preemptive().basic(credentials.getTestUser(), credentials.getTestPassword())
						.body(newUser)
						.when()
						.delete("http://localhost:8081/waesheroes/api/v1/users")
						.then().log().status()
						.statusCode(HttpStatus.SC_OK)
						.extract().response();
				response.prettyPrint();
			}
		}
		
		isFailScenario = false;
		isDummyExist = false;

	}
}
