package testpackage;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import common.Credentials;

public class TestBase {

	protected Credentials credentials;

	public static final Logger LOG = LoggerFactory.getLogger(TestBase.class);
	
	@BeforeClass
	public void beforeTest() {
		
		LOG.info("=================== Test Set Begins ===================");
		try {
			credentials = new Credentials();
		} catch (FileNotFoundException e) {
			System.err.println("Credential file is not located");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public void afterTest() {
		LOG.info("=================== Test Set Ends ===================");
	}
	
	@BeforeMethod(alwaysRun = true)
	public void beforeMethod() {
		LOG.info("=================== Test Begins ===================");
	}
	
	@AfterMethod(alwaysRun = true)
	public void AfterMethod() {
		LOG.info("=================== Test Ends ===================");
	}
}
